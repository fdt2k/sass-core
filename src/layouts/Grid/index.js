import { identity, is_array } from '@karsegard/composite-js';
import { bem, camelize, compose, divElement, withBaseClass, withModifiers, withVariables } from '@karsegard/react-compose';
import './grid.scss'; 


const modifiers = [
    'r3c1',
    'r3c3',
    'cover',
    'grow',
    'contained',
    'scrollable',
    'fit'
]

const withTemplateAreas = withVariables(
  _ => `gridTemplateAreas`,
  x => {
    if(is_array(x)){
      return x.map(item=> `"${item}"`).join(' ')
    }else {
      return`"${x}"`;
    }
  },
  ['templateAreas']
)

const withTemplateColRow = withVariables(
  x=> camelize(`grid-${x}`),
  identity,
  ['templateColumns','templateRows','autoRows']
)

const withGaps = withVariables(
  x=> camelize(x),
  identity,
  ['rowGap','columnGap']
)

const [__base_class,element,modifier] = bem ('layout-grid')

const Grid = compose(
    withTemplateAreas,
    withTemplateColRow,
    withGaps,
    withModifiers(x => modifier(x), modifiers),
    withBaseClass(__base_class)
)(divElement)



export const withGridArea =  withVariables(
    _ => `gridArea`,
    x => `${x}`,
    ['area']
);


export const ComponentWithArea = compose(
    withGridArea
)(divElement);






export default Grid;
